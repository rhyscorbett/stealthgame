using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Break : MonoBehaviour
{
    

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Melee")
            Destroy(gameObject);
    }
}
