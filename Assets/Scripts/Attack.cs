using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }


    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
            anim.SetBool("attacking", true);
        else if (Input.GetButtonUp("Fire1"))
            anim.SetBool("attacking", false);
    }
}
